<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class UsersController extends AppController
{	
	public function beforeFilter(Event $event)
    {

    }

    public function index() {
        $users = $this->Users->find('all', [
            'order' => ['Users.id' => 'ASC']
        ])->contain(['Companies']);
        $this->set('users', $users);
    }

    public function add() {
        
        if ($this->request->is('post', 'put')) {
            $user = $this->Users->newEntity();
            $user = $this->Users->patchEntity($user, $this->request->data);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else { 
                $this->Flash->error(__('Invalid E-mail.'));
            }
            $this->set('user', $user);
        } else if ($this->request->is('ajax')) {
            $this->loadModel('Companies');
            $companies = $this->Companies->find('list');
            $this->set('companies', $companies);
            $this->viewBuilder()->autoLayout(false);
        }
    }

    public function edit() {
        
        $id = $_GET['id'];
        $user = $this->Users->find()->where(['id' => $id])->first();

        if (!$user)
          throw new NotFoundException(__('Company not found.'));

        if ($this->request->is('post', 'put')) {
            $user = $this->Users->patchEntity($user, $this->request->data);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been updated.'));
                return $this->redirect(['action' => 'index']);
            } else { 
                $this->Flash->error(__('Something went wrong.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->set('user', $user);
        } else if ($this->request->is('ajax')) {
            $this->loadModel('Companies');
            $companies = $this->Companies->find('list');
            $this->set('companies', $companies);
            $this->set('user', $user);
            $this->viewBuilder()->autoLayout(false);
        }

    }

    public function remove() {
        
        if ($this->request->is('ajax')) {
            $id = $_GET['id'];
            $entity = $this->Users->get($id);
            if ($entity) {
                $this->Users->delete($entity);
                $users = $this->Users->find('all', [
                    'order' => ['Users.id' => 'ASC']
                ]);
                $this->loadModel('Companies');
                $companies = $this->Companies->find('list');
                $this->set('companies', $companies);
                $this->set('users', $users);
                $this->viewBuilder()->autoLayout(false)->template('index');
            } else {
                return false;
            }
        }

    }
}