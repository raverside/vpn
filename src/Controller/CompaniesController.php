<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class CompaniesController extends AppController
{	
	public function index() {
		$companies = $this->Companies->find('all', [
            'order' => ['Companies.id' => 'ASC']
        ]);
        $this->set('companies', $companies);
	}

	public function add() {
        
        if ($this->request->is('post', 'put')) {
            $company = $this->Companies->newEntity();
            $company = $this->Companies->patchEntity($company, $this->request->data);

            if ($this->Companies->save($company)) {
                $this->Flash->success(__('The company has been created.'));
                return $this->redirect(['action' => 'index']);
            } else { 
                $this->Flash->error(__('Company already exists.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->set('company', $company);
        } else if ($this->request->is('ajax')) {
            $this->viewBuilder()->autoLayout(false);
        }

    }

    public function edit() {

    	$id = $_GET['id'];
    	$company = $this->Companies->find()->where(['id' => $id])->first();

        if (!$company)
          throw new NotFoundException(__('Company not found.'));

    	if ($this->request->is('post', 'put')) {
            $company = $this->Companies->patchEntity($company, $this->request->data);

            if ($this->Companies->save($company)) {
                $this->Flash->success(__('The company has been updated.'));
                return $this->redirect(['action' => 'index']);
            } else { 
                $this->Flash->error(__('Something went wrong.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->set('company', $company);
        } else if ($this->request->is('ajax')) {
            $this->set('company', $company);
	    	$this->viewBuilder()->autoLayout(false);
        }

    }

    public function remove() {

    	if ($this->request->is('ajax')) {
    		$id = $_GET['id'];
    		$entity = $this->Companies->get($id);
    		if ($entity) {
	    		$this->Companies->delete($entity);
	    		$companies = $this->Companies->find('all', [
		            'order' => ['Companies.id' => 'ASC']
		        ]);
		        $this->set('companies', $companies);
	    		$this->viewBuilder()->autoLayout(false)->template('index');
	    	} else {
	    		return false;
	    	}
    	}

    }
}