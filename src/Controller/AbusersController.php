<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

class AbusersController extends AppController
{	
	public function index() {
		$prevmonth = date('m', strtotime('-1 month'));
		for ($i = 1; $i <= 6; $i++) {
			$months[$prevmonth] = date("F", strtotime( date( 'Y-m-01' )." -$i months"));
			$prevmonth--;
			if ($prevmonth == 0) {
				$prevmonth = 12;
			}
		}
		$this->set('months', $months);

	}

	public function generate() {
		$conn = ConnectionManager::get('default');
		$firstName = $conn->execute('TRUNCATE TABLE transfers');
		//^sadly there's no better way to clear the table in cakePHP

		$this->loadModel('Users');
		$users = $this->Users->find('all');
		$first  = strtotime('first day this month');
		$months = array();

		for ($i = 6; $i >= 1; $i--) {
		  	$month = date('m-Y', strtotime("-$i month", $first));

		  	foreach ($users as $user) {
		  		for ($x = 0; $x <= mt_rand(1, 50) ; $x++) {
		  			$date = mt_rand(1, date('t',strtotime('1-'.$month))).'-'.$month;
		  			$transfered = mt_rand(100, 10995116277760);

		  			$transfer = [
		  				'user_id' => $user['id'],
		  				'date' => date('Y-m-d', strtotime($date)),
		  				'resource' => 'http://xxxx.xxx/xx.xx',
		  				'transfered' => $transfered
		  			];
		  			
		  			$record = $this->Abusers->newEntity();
		  			$record = $this->Abusers->patchEntity($record, $transfer);

		  			$this->Abusers->save($record);
		  		}
			}
		}
		exit;
	}

	public function report() {

		if ($_GET['month']) {
			$month = $_GET['month'];
			$this->loadModel('Companies');
			$companies = $this->Companies->find('all', ['contain' => ['Users' => ['Abusers' => ['conditions' => ['MONTH(date)' => $month]]]]]);
			$abusers = [];
			foreach ($companies as $company) {
				$used = 0;
				foreach ($company['users'] as $user) {
					foreach ($user['abusers'] as $transfer) {
						$used += $this->byteFormat($transfer['transfered'], 'TB', 0);
					}
				}
				if ($company['quota'] < $used) {
					$abuser = [
						'name' => $company['company'],
						'used' => $used,
						'quota' =>$company['quota']
					];
					array_push($abusers, $abuser);
				}
			}

			usort($abusers, function($a, $b) {
			    return $b['used'] - $a['used'];
			});

			$this->viewBuilder()->autoLayout(false);
			$this->set('abusers', $abusers);
		}

	}

	private function byteFormat($bytes, $unit = "", $decimals = 2) {
		$units = array('B' => 0, 'KB' => 1, 'MB' => 2, 'GB' => 3, 'TB' => 4, 
				'PB' => 5, 'EB' => 6, 'ZB' => 7, 'YB' => 8);

		$value = 0;
		if ($bytes > 0) {
			// Generate automatic prefix by bytes 
			// If wrong prefix given
			if (!array_key_exists($unit, $units)) {
				$pow = floor(log($bytes)/log(1024));
				$unit = array_search($pow, $units);
			}
		
			// Calculate byte value by prefix
			$value = ($bytes/pow(1024,floor($units[$unit])));
		}

		// If decimals is not numeric or decimals is less than 0 
		// then set default value
		if (!is_numeric($decimals) || $decimals < 0) {
			$decimals = 2;
		}

		// Format output
		return sprintf('%.' . $decimals . 'f '.$unit, $value);
  	}
}