<?php  
echo $this->Form->create(null, ['id' => 'create-user']);
echo $this->Form->input(__('First name'), array('name' => 'first_name', 'required' => 'required'));
echo $this->Form->input(__('Last name'), array('name' => 'last_name', 'required'=>'required'));
echo $this->Form->input(__('Email'), array('name' => 'email', 'required'=>'required'));
echo $this->Form->input(__('Company'), ['name' => 'company', 'options' => $companies]);
echo $this->Form->submit(__('Submit'));
echo $this->Form->end();