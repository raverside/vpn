<div class="container">
	<a href="/users/add" id="user-create" class="create-link"><div class="create-block"><?=__('New User')?></div></a>
	<table class="users-table">
		<thead>
			<tr>
				<th><?=__('First Name')?></th>
				<th><?=__('Last Name')?></th>
				<th><?=__('Email')?></th>
				<th><?=__('Company')?></th>
				<th><?=__('Edit')?></th>
				<th><?=__('Remove')?></th>
			</tr>
		</thead>
		<tbody>
	<?php foreach ($users as $user) { ?>
		<tr>
			<td><?=__($user['first_name'])?></td>
			<td><?=__($user['last_name'])?></td>
			<td><?=__($user['email'])?></td>
			<td><?=__($user['company_name']['company'])?></td>
			<td><a href="/users/edit?id=<?=$user['id']?>" class="edit"><?=__('Edit')?></a></td>
			<td><a href="/users/remove?id=<?=$user['id']?>" class="remove"><?=__('Remove')?></a></td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
</div>