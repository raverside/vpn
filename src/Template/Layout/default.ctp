<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'VPN';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,500&amp;subset=cyrillic" rel="stylesheet">

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <?= $this->Html->script('jquery-3.1.1.min');?>
    <?= $this->Html->script('jquery.cookie');?>
    <?= $this->Html->script('script');?>
</head>
<body class="<?=(isset($admin)) ? $admin : '' ?>">
    <div id="modal">
        <div id="modal-shadow"></div>
        <div id="modal-form"></div>
    </div>
    <div class="header">
    	<div class="container">
    		<a href="/users" class="dir"><?=__('Users')?></a>
    		<a href="/companies" class="dir"><?=__('Companies')?></a>
    		<a href="/abusers" class="dir"><?=__('Abusers')?></a>
        </div>
    </div>
    <?= $this->Flash->render() ?>
    <div id="content"><?= $this->fetch('content') ?></div>
    <div class="footer">

    </div>
</body>
</html>
