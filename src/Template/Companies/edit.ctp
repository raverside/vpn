<?php  
echo $this->Form->create(null, ['id' => 'create-company']);
echo $this->Form->input(__('Company'), array('name' => 'company', 'required' => 'required', 'value' => $company['company']));
echo $this->Form->input(__('Quota (in TB)'), array('name' => 'quota', 'required'=>'required', 'value' => $company['quota']));
echo $this->Form->submit(__('Submit'));
echo $this->Form->end();