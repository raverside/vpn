<div class="container">
	<a href="/companies/add" id="company-create" class="create-link"><div class="create-block"><?=__('New Company')?></div></a>
	<table class="company-table">
		<thead>
			<tr>
				<th><?=__('Company')?></th>
				<th><?=__('Quota')?></th>
				<th><?=__('Edit')?></th>
				<th><?=__('Remove')?></th>
			</tr>
		</thead>
		<tbody>
	<?php foreach ($companies as $company) { ?>
		<tr>
			<td><?=__($company['company'])?></td>
			<td><?=__($company['quota'])?> TB</td>
			<td><a href="/companies/edit?id=<?=$company['id']?>" class="edit"><?=__('Edit')?></a></td>
			<td><a href="/companies/remove?id=<?=$company['id']?>" class="remove"><?=__('Remove')?></a></td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
</div>