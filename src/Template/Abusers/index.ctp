<div class="container">
	<a href="/abusers/generate" class="redbtn" id="generate">Generate data</a>
	<?=$this->Form->month('month', ['empty' => false, 'class' => 'select-month', 'id' => 'month', 'monthNames' => $months]);?>
	<a href="/abusers/report" class="redbtn" id="report">Show report</a>
	<div id="abusers-content"></div>
</div>