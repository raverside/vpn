<table class="abusers-table">
	<thead>
		<tr>
			<th><?=__('Company')?></th>
			<th><?=__('Used')?></th>
			<th><?=__('Quota')?></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($abusers as $abuser) { ?>
		<tr>
			<td><?=__($abuser['name'])?></td>
			<td><?=__($abuser['used'])?></td>
			<td><?=__($abuser['quota'])?></td>
		</tr>
	<?php } ?>
	</tbody>
</table>