$(document).ready(function(){
	//build the page
	$('.header .dir').css('padding', '5px 20px');
	$('.header .dir').css('font-size', '19px');
	$('.case').each(function(){
		// $(this).find('img').css('width', '50%');
		$(this).css('margin', '0');
	});
		setTimeout(function(){
			// $('.case').addClass('loaded');
			$('.showcase .case .description').fadeIn(3600);
		}, 1500);
	if ($('.message').length > 0) { 
		setTimeout(function(){
			$('.message').animate({
				width: '0',
				height: '0',
				padding: '0'
			},500);
			$('.message').fadeOut(500);
		}, 500);
	} 
	//please ignore the code above
	//I was trying to build something else and I don't feel like getting rid of it just yet
	//it might be useful later on

	
	$('#user-create').on('click', function(e){
		e.preventDefault();
		$('#modal').fadeIn();
		$.ajax({ 
	        type: 'GET', 
	        url: '/users/add',  
	        success: function (data) { 
	            $('#modal-form').html(data);
	        }
	    });
	});

	$('#company-create').on('click', function(e){
		e.preventDefault();
		$('#modal').fadeIn();
		$.ajax({ 
	        type: 'GET', 
	        url: '/companies/add',  
	        success: function (data) { 
	            $('#modal-form').html(data);
	        }
	    });
	});
	
	$('#modal-shadow').click(function() {
		$('#modal').fadeOut();
	});
	$('.remove').on('click', function(e){
		e.preventDefault();
		if (confirm('Are you sure?')) {
			$.ajax({ 
	        type: 'GET', 
	        url: $(this).attr('href'),  
	        success: function (data) { 
	            $('#content').html(data);
	        }
	    	});
		} else {
			return false;
		}
	});
	$('.edit').on('click', function(e) {
		e.preventDefault();
		$('#modal').fadeIn();
		$.ajax({ 
	        type: 'GET', 
	        url: $(this).attr('href'),  
	        success: function (data) { 
	            $('#modal-form').html(data);
	        }
	    });
	});
	$('#generate').on('click', function(e) {
		e.preventDefault();
		$('#modal').fadeIn();
		$('#modal-form').html('<div class="preloader"></div>');
		$.ajax({
			type: 'GET',
			url: $(this).attr('href'),
			success: function (data) {
				$('#modal-form').html('');
				$('#modal').fadeOut();
			}
		});
	});
	$('#report').on('click', function(e) {
		e.preventDefault();
		$.ajax({
			type: 'GET',
			url: $(this).attr('href')+'?month='+$('#month').val(),
			success: function (data) {
				$('#abusers-content').html(data);
			}
		});
	});
});